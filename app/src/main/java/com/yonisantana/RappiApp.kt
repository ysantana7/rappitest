package com.yonisantana

import android.app.Application
import com.yonisantana.di.components.ApplicationComponent
import com.yonisantana.di.components.DaggerApplicationComponent
import com.yonisantana.di.modules.ApplicationModule

class RappiApp : Application() {

    companion object {
        @JvmStatic
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDependencies()
    }

    private fun initDependencies(){
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}