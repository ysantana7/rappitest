package com.yonisantana.ui.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        initFragment()
    }

    @LayoutRes
    abstract fun getLayout(): Int

    @IdRes
    abstract fun getContainer(): Int

    abstract fun getInitialFragment(): Fragment?

    private fun initFragment() {
        if (getLayout() != 0) {
            getInitialFragment()?.let { fm ->
                val ft = supportFragmentManager.beginTransaction()
                ft.add(getContainer(), fm)
                ft.commitAllowingStateLoss()
            }
        }
    }
}