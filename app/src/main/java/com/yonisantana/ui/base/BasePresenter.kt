package com.yonisantana.ui.base

abstract class BasePresenter<V : BaseView> {

    private var view: V? = null

    fun setView(view: V) {
        this.view = view
    }

    fun getView(): V? = view

    abstract fun pause()

}