package com.yonisantana.ui.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
    fun onError(error: String)
}