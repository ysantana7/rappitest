package com.yonisantana.ui.movies

import com.yonisantana.domain.interactors.MoviesInteractor
import com.yonisantana.domain.models.MovieResponse
import com.yonisantana.ui.base.BasePresenter
import javax.inject.Inject

class MoviesPresenter @Inject constructor(private val moviesInteractor: MoviesInteractor) :
    BasePresenter<MoviesView>(), MoviesInteractor.MoviesListener {

    init {
        moviesInteractor.setMoviesListener(this)
    }

    fun getMoviesPopular() {
        getView()?.let {
            it.showLoading()
            moviesInteractor.popular()
        }
    }

    fun getMoviesTopRated() {
        getView()?.let {
            it.showLoading()
            moviesInteractor.topRated()
        }
    }

    fun getMoviesUpcoming() {
        getView()?.let {
            it.showLoading()
            moviesInteractor.upcoming()
        }
    }

    override fun onMoviesSuccess(response: MovieResponse) {
        getView()?.let {
            it.onMoviesSuccess(response)
            it.hideLoading()
        }
    }

    override fun onMoviesError(error: String) {
        getView()?.let {
            it.hideLoading()
            it.onError(error)
        }
    }

    override fun pause() {
        moviesInteractor?.stop()
    }
}