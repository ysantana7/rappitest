package com.yonisantana.ui.movies

import androidx.fragment.app.Fragment
import com.yonisantana.R
import com.yonisantana.ui.base.BaseActivity

class MoviesActivity : BaseActivity() {

    override fun getLayout(): Int = R.layout.activity_movies

    override fun getContainer(): Int = R.id.flContainer

    override fun getInitialFragment(): Fragment = MoviesFragment.newInstance()
}