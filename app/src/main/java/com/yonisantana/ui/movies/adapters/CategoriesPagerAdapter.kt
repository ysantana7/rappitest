package com.yonisantana.ui.movies.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.yonisantana.R
import com.yonisantana.ui.movies.PopularPagerFragment
import com.yonisantana.utils.Category

class CategoriesPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PopularPagerFragment.newInstance(Category.POPULAR)
            1 -> PopularPagerFragment.newInstance(Category.TOP_RATED)
            else -> PopularPagerFragment.newInstance(Category.UPCOMING)
        }
    }

    override fun getCount(): Int = 3

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> context.getString(R.string.popular)
            1 -> context.getString(R.string.top_rated)
            else -> context.getString(R.string.upcoming)
        }
    }
}