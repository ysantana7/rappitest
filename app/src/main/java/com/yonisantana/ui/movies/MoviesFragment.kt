package com.yonisantana.ui.movies

import android.os.Bundle
import android.view.View
import com.yonisantana.R
import com.yonisantana.ui.base.BaseFragment
import com.yonisantana.ui.movies.adapters.CategoriesPagerAdapter
import kotlinx.android.synthetic.main.fragment_movies.*

class MoviesFragment : BaseFragment() {

    companion object {
        fun newInstance() = MoviesFragment()
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        super.initView(view, savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        val mPagerAdapter = CategoriesPagerAdapter(activity!!, childFragmentManager)
        vpCategories.adapter = mPagerAdapter
        tblCategories.setupWithViewPager(vpCategories)
    }

    override fun getFrameLayout(): Int = R.layout.fragment_movies
}