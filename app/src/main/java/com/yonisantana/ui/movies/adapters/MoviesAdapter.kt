package com.yonisantana.ui.movies.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.yonisantana.R
import com.yonisantana.commons.loadUrl
import com.yonisantana.data.DataConfiguration
import com.yonisantana.domain.models.MovieModel
import com.yonisantana.ui.base.BaseAdapter
import kotlinx.android.synthetic.main.item_movie.view.*

class MoviesAdapter : BaseAdapter<MovieModel>() {

    override fun getLayoutId(position: Int, obj: MovieModel): Int = R.layout.item_movie

    override fun getViewHolder(view: View, viewType: Int) = ItemViewHolder(view)

    inner class ItemViewHolder(v: View) : RecyclerView.ViewHolder(v), Binder<MovieModel> {
        override fun bind(position: Int, data: MovieModel) {
            itemView.ivMovie.loadUrl("${DataConfiguration.URL_IMAGE200}/${data._posterPath}")
            itemView.txtTitle.text = data._title
        }
    }
}