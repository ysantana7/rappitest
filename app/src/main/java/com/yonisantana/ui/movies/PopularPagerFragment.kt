package com.yonisantana.ui.movies

import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.yonisantana.R
import com.yonisantana.RappiApp
import com.yonisantana.commons.gone
import com.yonisantana.commons.modeGrid
import com.yonisantana.commons.visible
import com.yonisantana.di.components.ApplicationComponent
import com.yonisantana.di.components.DaggerApplicationComponent
import com.yonisantana.di.modules.ApplicationModule
import com.yonisantana.domain.models.MovieResponse
import com.yonisantana.ui.base.BaseFragment
import com.yonisantana.ui.movies.adapters.MoviesAdapter
import com.yonisantana.utils.Category
import kotlinx.android.synthetic.main.fragment_popular.*
import javax.inject.Inject

class PopularPagerFragment : BaseFragment(), MoviesView {

    lateinit var moviesAdapter: MoviesAdapter

    @Inject
    lateinit var moviesPresenter: MoviesPresenter

    lateinit var applicationComponent: ApplicationComponent

    lateinit var category: Category

    companion object {
        fun newInstance(category: Category): PopularPagerFragment {
            return PopularPagerFragment().apply { this.category = category }
        }
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        super.initView(view, savedInstanceState)
        initInjectComponent()
        initAdapter()

        when (category) {
            Category.POPULAR -> {
                moviesPresenter.getMoviesPopular()
            }
            Category.TOP_RATED -> {
                moviesPresenter.getMoviesTopRated()
            }
            else -> {
                moviesPresenter.getMoviesUpcoming()
            }
        }
    }

    private fun initAdapter() {
        moviesAdapter = MoviesAdapter()
        rvMovies.modeGrid(3)
        rvMovies.adapter = moviesAdapter
    }

    override fun getFrameLayout(): Int = R.layout.fragment_popular

    override fun onMoviesSuccess(response: MovieResponse) {
        moviesAdapter.setItems(response._movies)
    }

    override fun showLoading() = progressBar.visible()

    override fun hideLoading() = progressBar.gone()

    override fun onError(error: String) {
        view?.let { Snackbar.make(it, error, Snackbar.LENGTH_LONG).show() }
    }

    private fun initInjectComponent() {
        if (!::applicationComponent.isInitialized) {
            applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(activity!!.application as RappiApp))
                .build()
        }
        applicationComponent.inject(this)
        moviesPresenter.setView(this)
    }
}