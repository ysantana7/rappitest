package com.yonisantana.ui.movies

import com.yonisantana.domain.models.MovieResponse
import com.yonisantana.ui.base.BaseView

interface MoviesView : BaseView {
    fun onMoviesSuccess(response: MovieResponse)
}