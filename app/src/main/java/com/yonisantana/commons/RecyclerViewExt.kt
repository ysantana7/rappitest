package com.yonisantana.commons

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.modeGrid(spanCount: Int) {
    layoutManager = GridLayoutManager(context, spanCount)
    setHasFixedSize(true)
}