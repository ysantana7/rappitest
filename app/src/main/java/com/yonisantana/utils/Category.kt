package com.yonisantana.utils

enum class Category {
    POPULAR, TOP_RATED, UPCOMING
}