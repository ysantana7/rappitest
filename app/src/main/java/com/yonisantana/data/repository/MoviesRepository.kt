package com.yonisantana.data.repository

import com.yonisantana.domain.models.MovieResponse
import rx.Observable

interface MoviesRepository {

    fun popular(): Observable<MovieResponse>

    fun topRated(): Observable<MovieResponse>

    fun upcoming(): Observable<MovieResponse>
}