package com.yonisantana.data.repository

import com.yonisantana.data.NetworkManager
import com.yonisantana.data.net.NetMoviesEntity
import com.yonisantana.data.net.services.MoviesService
import com.yonisantana.domain.exception.RappiNetworkException
import com.yonisantana.domain.models.MovieResponse
import rx.Observable
import javax.inject.Inject

class MoviesRepositoryFactory @Inject constructor(
    private val networkManager: NetworkManager,
    service: MoviesService
) : MoviesRepository {

    private var netMoviesEntity: NetMoviesEntity? = null

    init {
        netMoviesEntity = NetMoviesEntity(service)
    }

    override fun popular(): Observable<MovieResponse> {
        return if (networkManager.isNetworkAvailable()) {
            netMoviesEntity!!.popular()
        } else {
            Observable.error(RappiNetworkException())
        }
    }

    override fun topRated(): Observable<MovieResponse> {
        return if (networkManager.isNetworkAvailable()) {
            netMoviesEntity!!.topRated()
        } else {
            Observable.error(RappiNetworkException())
        }
    }

    override fun upcoming(): Observable<MovieResponse> {
        return if (networkManager.isNetworkAvailable()) {
            netMoviesEntity!!.upcoming()
        } else {
            Observable.error(RappiNetworkException())
        }
    }
}