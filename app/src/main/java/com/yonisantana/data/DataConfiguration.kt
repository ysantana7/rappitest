package com.yonisantana.data

class DataConfiguration {

    companion object {
        const val API_KEY = "ad28dc12362a8e676a4cbd0afe623621"
        const val URL_IMAGE200 = "https://image.tmdb.org/t/p/w200"
        const val BASE_URL = "http://api.themoviedb.org/3/movie/"
        const val POPULAR = "${BASE_URL}popular?api_key=${API_KEY}&language=en-US&page=1"
        const val TOP_RATED = "${BASE_URL}top_rated?api_key=${API_KEY}&language=en-US&page=1"
        const val UPCOMING = "${BASE_URL}upcoming?api_key=${API_KEY}&language=en-US&page=1"
    }

    fun getBaseUrl() = BASE_URL

}