package com.yonisantana.data.net.services

import com.yonisantana.data.DataConfiguration
import com.yonisantana.domain.models.MovieResponse
import retrofit2.Response
import retrofit2.http.GET
import rx.Observable

interface MoviesService {
    @GET(DataConfiguration.POPULAR)
    fun getPopular(): Observable<Response<MovieResponse>>

    @GET(DataConfiguration.TOP_RATED)
    fun getTopRated(): Observable<Response<MovieResponse>>

    @GET(DataConfiguration.UPCOMING)
    fun getUpcoming(): Observable<Response<MovieResponse>>
}