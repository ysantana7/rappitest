package com.yonisantana.data.net.models

import com.google.gson.annotations.SerializedName
import com.yonisantana.domain.models.BaseBean

class Error : BaseBean (){
    @SerializedName(value = "message")
    var message: String = ""
    @SerializedName(value = "code")
    var code: Int = 0
}