package com.yonisantana.data.net

import com.yonisantana.data.net.services.MoviesService
import com.yonisantana.data.net.utils.ResponseUtils
import com.yonisantana.data.repository.MoviesRepository
import com.yonisantana.domain.models.MovieResponse
import retrofit2.Response
import rx.Observable
import rx.functions.Func1

class NetMoviesEntity(private val service: MoviesService) : MoviesRepository {
    override fun popular(): Observable<MovieResponse> {
        return service.getPopular()
            .flatMap(Func1<Response<MovieResponse>, Observable<MovieResponse>> {
                if (it.isSuccessful) {
                    return@Func1 Observable.just(it.body()!!)
                } else {
                    return@Func1 Observable.error(ResponseUtils.processError(it))
                }
            })
    }

    override fun topRated(): Observable<MovieResponse> {
        return service.getTopRated()
            .flatMap(Func1<Response<MovieResponse>, Observable<MovieResponse>> {
                if (it.isSuccessful) {
                    return@Func1 Observable.just(it.body()!!)
                } else {
                    return@Func1 Observable.error(ResponseUtils.processError(it))
                }
            })
    }

    override fun upcoming(): Observable<MovieResponse> {
        return service.getUpcoming()
            .flatMap(Func1<Response<MovieResponse>, Observable<MovieResponse>> {
                if (it.isSuccessful) {
                    return@Func1 Observable.just(it.body()!!)
                } else {
                    return@Func1 Observable.error(ResponseUtils.processError(it))
                }
            })
    }
}