package com.yonisantana.data.net.utils

import android.util.Log
import com.google.gson.Gson
import com.yonisantana.domain.exception.RappiException
import com.yonisantana.domain.exception.RappiHttpException
import com.yonisantana.domain.exception.RappiNetworkException
import com.yonisantana.domain.exception.RappiUnknownHttpException
import retrofit2.Response
import com.yonisantana.data.net.models.Error

class ResponseUtils {
    companion object {

        private val TAG: String = ResponseUtils::class.java.simpleName

        fun processError(response: Response<*>): RappiException {
            try {
                val errorBody = response.errorBody()?.string() ?: ""
                Log.e(TAG, "processError()$errorBody")
                if (errorBody.isEmpty()) {
                    if (response.code() == 504) {
                        return RappiNetworkException(message = "Network error: $errorBody")
                    }
                    return RappiUnknownHttpException(
                        message = "Unknown error: $errorBody",
                        ex = null,
                        responseCode = response.code()
                    )
                } else {
                    val error = Gson().fromJson(errorBody, Error::class.java)
                    error.code = response.code()
                    return RappiHttpException(
                        message = "${error.message} $errorBody",
                        ex = null,
                        responseCode = response.code()
                    )
                }
            } catch (ex: Exception) {
                return RappiUnknownHttpException(
                    message = "Error parsing message: " + ex.message,
                    ex = ex,
                    responseCode = response.code()
                )
            }
        }
    }
}