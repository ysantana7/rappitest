package com.yonisantana.domain.exception

open class RappiException : Exception {
    constructor(ex: Exception) : super(ex)
    constructor(message: String = "") : super(message)
    constructor(message: String = "", ex: Throwable?) : super(message, ex)
}