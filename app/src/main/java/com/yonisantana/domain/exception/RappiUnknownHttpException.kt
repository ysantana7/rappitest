package com.yonisantana.domain.exception

open class RappiUnknownHttpException : RappiException {
    var responseCode: Int = -1

    constructor(message: String = "Unknown error") : super(message)
    constructor(message: String = "Unknown error", ex: Throwable?, responseCode: Int) : super(message, ex) {
        this.responseCode = responseCode
    }
}