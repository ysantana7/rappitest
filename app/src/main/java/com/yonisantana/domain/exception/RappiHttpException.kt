package com.yonisantana.domain.exception

open class RappiHttpException : RappiException {
    var responseCode: Int = -1

    constructor(message: String = "Http error") : super(message)
    constructor(message: String = "Http error", ex: Throwable?, responseCode: Int) : super(
        message,
        ex
    ) {
        this.responseCode = responseCode
    }
}