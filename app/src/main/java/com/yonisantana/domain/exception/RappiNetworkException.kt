package com.yonisantana.domain.exception

open class RappiNetworkException : RappiException {
    constructor(message: String = "Network error") : super(message)
}