package com.yonisantana.domain.models

import com.google.gson.annotations.SerializedName

open class MovieResponse(
    @SerializedName("page")
    var page: Int? = null,
    @SerializedName("total_pages")
    var totalPages: Int? = null,
    @SerializedName("results")
    var movies: List<MovieModel>? = null
) {
    val _page: Int get() = page ?: 0
    val _totalPages: Int get() = totalPages ?: 0
    val _movies: List<MovieModel> get() = movies ?: emptyList()
}

open class MovieModel(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("overview")
    var overview: String? = null,
    @SerializedName("poster_path")
    var posterPath: String? = null
) {
    val _id: Int get() = id ?: 0
    val _title: String get() = title ?: ""
    val _overview: String get() = overview ?: ""
    val _posterPath: String get() = posterPath ?: ""
}