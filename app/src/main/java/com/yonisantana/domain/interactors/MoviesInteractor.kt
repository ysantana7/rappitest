package com.yonisantana.domain.interactors

import com.yonisantana.data.repository.MoviesRepository
import com.yonisantana.domain.exception.RappiException
import com.yonisantana.domain.exception.RappiUnknownHttpException
import com.yonisantana.domain.models.MovieResponse
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesInteractor @Inject constructor(private val repository: MoviesRepository) {

    private var subscription: Subscription? = null
    private var moviesListener: MoviesListener? = null

    fun setMoviesListener(moviesListener: MoviesListener) {
        this.moviesListener = moviesListener
    }

    fun popular() {
        subscription = repository.popular()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                moviesListener?.onMoviesSuccess(it)
            }, {
                moviesListener?.onMoviesError(parseException(it).message!!)
            })
    }

    fun topRated() {
        subscription = repository.topRated()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                moviesListener?.onMoviesSuccess(it)
            }, {
                moviesListener?.onMoviesError(parseException(it).message!!)
            })
    }

    fun upcoming() {
        subscription = repository.upcoming()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                moviesListener?.onMoviesSuccess(it)
            }, {
                moviesListener?.onMoviesError(parseException(it).message!!)
            })
    }

    fun stop() {
        subscription?.unsubscribe()
    }

    private fun parseException(throwable: Throwable) =
        if (throwable is IOException) RappiUnknownHttpException() else throwable as RappiException

    interface MoviesListener {
        fun onMoviesSuccess(response: MovieResponse)
        fun onMoviesError(error: String)
    }
}