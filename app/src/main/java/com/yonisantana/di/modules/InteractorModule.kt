package com.yonisantana.di.modules

import com.yonisantana.data.repository.MoviesRepository
import com.yonisantana.domain.interactors.MoviesInteractor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class InteractorModule {

    @Provides
    @Singleton
    fun providerMoviesInteractor(moviesRepository: MoviesRepository): MoviesInteractor {
        return MoviesInteractor(moviesRepository)
    }
}