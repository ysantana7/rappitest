package com.yonisantana.di.modules

import com.yonisantana.data.NetworkManager
import com.yonisantana.data.net.services.MoviesService
import com.yonisantana.data.repository.MoviesRepository
import com.yonisantana.data.repository.MoviesRepositoryFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DataModule {
    @Provides
    @Singleton
    fun providesMoviesRepository(
        retrofit: Retrofit,
        networkManager: NetworkManager
    ): MoviesRepository {
        return MoviesRepositoryFactory(networkManager, retrofit.create(MoviesService::class.java))
    }
}