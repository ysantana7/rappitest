package com.yonisantana.di.modules

import android.content.Context
import com.yonisantana.RappiApp
import com.yonisantana.data.DataConfiguration
import com.yonisantana.data.NetworkManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val rappiApp: RappiApp) {

    @Provides
    @Singleton
    fun providesApplication(): RappiApp = rappiApp

    @Provides
    @Singleton
    fun providesContext(): Context {
        return rappiApp.applicationContext
    }

    @Provides
    @Singleton
    fun providesDataConfiguration(): DataConfiguration = DataConfiguration()

    @Provides
    @Singleton
    fun providesNetworkManager(): NetworkManager = NetworkManager(rappiApp)

}