package com.yonisantana.di.components

import com.yonisantana.di.modules.ApplicationModule
import com.yonisantana.di.modules.DataModule
import com.yonisantana.di.modules.InteractorModule
import com.yonisantana.di.modules.NetworkModule
import com.yonisantana.ui.movies.PopularPagerFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(DataModule::class), (NetworkModule::class), (InteractorModule::class), (ApplicationModule::class)])
interface ApplicationComponent {
    fun inject(popularFragment: PopularPagerFragment)
}